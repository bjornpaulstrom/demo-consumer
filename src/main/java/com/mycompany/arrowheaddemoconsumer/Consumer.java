package com.mycompany.arrowheaddemoconsumer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Björn
 */
public class Consumer {

    public static void main(String[] args) {

        String message;
        JSONObject body = new JSONObject();
        JSONObject empty = new JSONObject();
        JSONArray emptyArray = new JSONArray();

        body.put("interfaceRequirements", emptyArray);
        body.put("securityRequirements", emptyArray);
        body.put("metadataRequirements", empty);
        body.put("serviceDefinitionRequirement", "our-service-type");

        message = body.toString();
        System.out.println(message);

        URL obj = null;
        try {
            obj = new URL("http://192.168.54.175:8443/serviceregistry/query");
        } catch (MalformedURLException ex) {
            Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (obj == null) {
            return;
        }

        HttpURLConnection con = null;

        try {
            con = (HttpURLConnection) obj.openConnection();
        } catch (IOException ex) {
            Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (con == null) {
            return;
        }

        con.setRequestProperty("Content-Type", "application/json");

        try {
            con.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
        }

        con.setDoOutput(true);
        OutputStream os = null;
        try {
            os = con.getOutputStream();
        } catch (IOException ex) {
            Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (os == null) {
            return;
        }

        try {
            os.write(message.getBytes());
            os.flush();
            os.close();
        } catch (IOException ex) {
            Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
        }

        int responseCode = 0;
        try {
            responseCode = con.getResponseCode();
        } catch (IOException ex) {
            Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("POST Response Code :: " + responseCode);

        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        } catch (IOException ex) {
            Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
        }
        String inputLine;
        StringBuilder response = new StringBuilder();

        if (in == null) {
            return;
        }

        try {
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (IOException ex) {
            Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
        }

        // print result
        System.out.println(response.toString());

        JSONObject responseObject = new JSONObject(response.toString());
        JSONArray serviceQueryData = responseObject.getJSONArray("serviceQueryData");

        JSONObject queryResult = (JSONObject) serviceQueryData.get(0);

        JSONObject provider = queryResult.getJSONObject("provider");
        String remoteIpAddress = provider.getString("address");
        int remotePort = provider.getInt("port");
        String serviceUri = queryResult.getString("serviceUri");

        String uri = "http://" + remoteIpAddress + ":" + remotePort + serviceUri;

        System.out.println("Found resource located at: " + uri);

        sendPost(uri);
    }

    private static void sendPost(String uri) {
        URL obj = null;
        try {
            obj = new URL(uri);
        } catch (MalformedURLException ex) {
            Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (obj == null) {
            return;
        }

        HttpURLConnection con = null;

        try {
            con = (HttpURLConnection) obj.openConnection();
        } catch (IOException ex) {
            Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (con == null) {
            return;
        }

        con.setRequestProperty("Content-Type", "application/json");

        try {
            con.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
        }

        con.setDoOutput(true);
        OutputStream os = null;
        try {
            os = con.getOutputStream();
        } catch (IOException ex) {
            Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (os == null) {
            return;
        }

        JSONObject empty = new JSONObject();

        try {
            os.write(empty.toString().getBytes());
            os.flush();
            os.close();
        } catch (IOException ex) {
            Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
        }

        int responseCode = 0;
        try {
            responseCode = con.getResponseCode();
        } catch (IOException ex) {
            Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("POST Response Code: " + responseCode);

        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        } catch (IOException ex) {
            Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
        }
        String inputLine;
        StringBuilder response = new StringBuilder();

        if (in == null) {
            return;
        }

        try {
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (IOException ex) {
            Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println(response.toString());
    }
}
